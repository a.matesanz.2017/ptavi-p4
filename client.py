#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Programa cliente UDP que abre un socket a un servidor
"""

import socket
import sys

if len(sys.argv) != 6:
    sys.exit("Usage: client.py ip puerto register sip_address expires_value")

# client.py localhost 6001 register sip_address 3600
_, SERVER, PORT, METHODD, ADDRESS, EXPIRESS = sys.argv

PORT = int(PORT)
METHODDU = METHODD.upper()
EXPIRESS = str(EXPIRESS)

# Creamos el socket, lo configuramos y lo atamos a un servidor/puerto
with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
    my_socket.connect((SERVER, PORT))

    print('Enviando:', METHODDU + ' sip:' + ADDRESS + ' SIP/2.0')
    print('Expires:', EXPIRESS)

    chainn1 = METHODDU + ' sip:' + ADDRESS + ' SIP/2.0\r\n'

    chainn2 = 'Expires: ' + EXPIRESS + '\r\n\r\n'

    my_socket.send(bytes(chainn1, 'utf-8') + bytes(chainn2, 'utf-8') + b'\r\n\r\n')

    data = my_socket.recv(1024)
    print('Recibido -- ', data.decode('utf-8'))

print("Socket terminado.")
