#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""

import socketserver
import sys
import time
import json

class SIPRegisterHandler(socketserver.DatagramRequestHandler):
    """
    Echo server class
    """

    reg_dicc = {}

    def register2json(self):
        """
        Paso a json
        """
        with open('registered.json', 'w') as f:
            json.dump(self.reg_dicc, f, indent=4)

    def handle(self):
        """
        handle method of the server class
        (all requests will be handled by this method)
        """
        self.wfile.write(b"SIP/2.0 200 OK\r\n\r\n")

        """
        lo que manda el cliente lo ponemos todo en un string
        """
        chainn = ''
        for line in self.rfile:
            ld = line.decode('utf-8')
            chainn = chainn + ld

        print("El cliente nos manda", chainn)
        if chainn == '\r\n':
            print("No se han encontrado datos de cliente")

        # Me quedo con los datos que me interesan del string
        else:
            chainn = chainn.split(' ')
            methodd = chainn[0]
            address = chainn[1]
            expiress = chainn[3]

            if methodd == 'REGISTER':

                IP = self.client_address[0]

                address = address.split(':')
                user_address = address[1]

                """
                Obtengo el tiempo final (cuando expira) y el actual para compararlos despues
                """
                t = time.time() + int(expiress)
                final_time = time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime(t))
                actual_time = time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime(time.time()))

                self.reg_dicc[user_address] = {'address': IP, 'expires': final_time}
                print(self.reg_dicc)

                delett = []

                """
                Recorro diccionario para ir actualizando la hora y posteriormente borrar
                """
                for user_address in self.reg_dicc:
                    if actual_time >= self.reg_dicc[user_address]['expires']:
                        delett.append(user_address)

                for user_address in delett:
                    del self.reg_dicc[user_address]

                self.register2json()


if __name__ == "__main__":
    # Listens at localhost ('') port 6001
    # and calls the EchoHandler class to manage the request

    if len(sys.argv) != 2:
        sys.exit("Too much arguments")

    # server.py 6001
    _, PORT = sys.argv

    PORT = int(PORT)

    serv = socketserver.UDPServer(('', PORT), SIPRegisterHandler) 

    print("Lanzando servidor UDP de eco...")
    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Finalizado servidor")
